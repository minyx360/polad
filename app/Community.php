<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use App\Lga;

class Community extends Model
{
    use \Znck\Eloquent\Traits\BelongsToThrough;
    use Searchable;

    public function toSearchableArray()
    {
        return [
            'name' => $this->name,
            'ruler_name' => $this->ruler_name,
            'lga_id' => $this->lga_id,
        ];
    }
    
    public function getLgaIdAttribute($value)
    {

        $lga = Lga::find($value);
        return $lga->name;
    }

    public function scopeCommunityId($query, $name)
    {

        $query->where('name', $name);
        return $this->id;
    }

    public function banks(){
       return $this->hasMany(Bank::class);
    }

    public function education(){
        return $this->hasMany(Education::class);
    }

    public function electricity(){
        return $this->hasMany(Electricity::class);
    }

    public function accessibility(){
        return $this->hasMany(Accessibility::class);
    }

    public function health(){
        return $this->hasMany(Health::class);
    }

    public function communication(){
        return $this->hasMany(Communication::class);
    }

    public function water(){
        return $this->hasMany(Water::class);
    }

    public function agroBasedCottage(){
        return $this->hasMany(AgroBasedCottage::class);
    }

    public function agricultureFacility(){
        return $this->hasMany(AgricultureFacility::class);
    }

    public function mineralBasedCottage(){
        return $this->hasMany(MineralBasedCottage::class);
    }

    public function otherSocialAmenity(){
        return $this->hasMany(OtherSocialAmenity::class);
    }

    public function tourismRecreation(){
        return $this->hasMany(TourismRecreation::class);
    }

    public function governmentAgency(){
        return $this->hasMany(GovernmentAgency::class);
    }

    public function lga(){

        return $this->belongsTo(Lga::class);
    }
}
