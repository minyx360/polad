<?php

namespace App\Http\Controllers\Api;

use App\Lga;
use App\Bank;
use App\Education;
use App\AgroBasedCottage;
use App\AgricultureFacility;
use App\GovernmentAgency;
use App\Communication;
use App\Accessibility;
use App\Health;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Water;
use App\Electricity;
use App\TourismRecreation;
use App\MineralBasedCottage;
use App\OtherSocialAmenity;

class DataListController extends Controller
{

    private $industry = ['banks' => 'Banks', 'educations' => 'Education', 'accessibilities' => 'Accessibilities', 'healths' => 'Hospital', 'communications' => 'Communications', 'electricities' => 'Electricities', 'waters' => 'Water', 'agroBasedCottages' => 'Agro Based Cottages', 'agricultureFacilities' => 'Agriculture Facilities', 'mineralBasedCottages' => 'Mineral Based Cottages', 'otherSocialAmenities' => 'Other Social Amenities', 'tourismRecreations' => 'Tourism Recreations', 'governmentAgencies' => 'Government Agencies'];



    public function getLgaList(){
        return collect(Lga::pluck('name', 'id'));
    }

    public function getIndustryList()
    {
        return $this->industry;
    }

    public function getIndustryCategory($cat)
    {
        switch($cat){
            case 'banks':
                $collection = collect(Bank::pluck('category', 'id'));
                break;
            case 'educations':
                $collection = collect(Education::pluck('category', 'id'));
                break;
            case 'communications':
                $collection = collect(Communication::pluck('category', 'id'));
                break;
            case 'healths':
                $collection = collect(Health::pluck('category', 'id'));
                break;
            case 'accessibilities':
                $collection = collect(Accessibility::pluck('category', 'id'));
                break;
            case 'electricities':
                $collection = collect(Electricity::pluck('category', 'id'));
                break;
            case 'waters':
                $collection = collect(Water::pluck('category', 'id'));
                break;
            case 'agricultureFacilities':
                $collection = collect(AgricultureFacility::pluck('category', 'id'));
                break;
            case 'agroBasedCottages':
                $collection = collect(AgroBasedCottage::pluck('category', 'id'));
                break;
            case 'tourismRecreations':
                $collection = collect(TourismRecreation::pluck('category', 'id'));
                break;
            case 'mineralBasedCottages':
                $collection = collect(MineralBasedCottage::pluck('category', 'id'));
                break;
            case 'otherSocialAmenities':
                $collection = collect(OtherSocialAmenity::pluck('category', 'id'));
                break;
            case 'governmentAgencies':
                $collection = collect(GovernmentAgency::pluck('category', 'id'));
                break;
        }


        $list = $collection->toArray();
        $list =  array_unique($list);
        return response()->json($list);
    }

     public function getOwnershipType($cat)
    {
        switch($cat){
            case 'banks':
                $collection = collect(Bank::pluck('ownership_type', 'id'));
                break;
            case 'educations':
                $collection = collect(Education::pluck('ownership_type', 'id'));
                break;
            case 'healths':
                $collection = collect(Health::pluck('ownership_type', 'id'));
                break;
            case 'accessibilities':
                $collection = collect(Accessibility::pluck('ownership_type', 'id'));
                break;
            case 'communications':
                $collection = collect(Communication::pluck('ownership_type', 'id'));
                break;
            case 'electricities':
                $collection = collect(Electricity::pluck('ownership_type', 'id'));
                break;
            case 'waters':
                $collection = collect(Water::pluck('ownership_type', 'id'));
                break;
            case 'agricultureFacilities':
                $collection = collect(AgricultureFacility::pluck('ownership_type', 'id'));
                break;
            case 'agroBasedCottages':
                $collection = collect(AgroBasedCottage::pluck('ownership_type', 'id'));
                break;
            case 'tourismRecreations':
                $collection = collect(TourismRecreation::pluck('ownership_type', 'id'));
                break;
            case 'mineralBasedCottages':
                $collection = collect(MineralBasedCottage::pluck('ownership_type', 'id'));
                break;
            case 'otherSocialAmenities':
                $collection = collect(OtherSocialAmenity::pluck('ownership_type', 'id'));
                break;
            case 'governmentAgencies':
                $collection = collect(GovernmentAgency::pluck('ownership_type', 'id'));
                break;
        }


        $list = $collection->toArray();
        $list =  array_unique($list);
        return response()->json($list);
    }
}
