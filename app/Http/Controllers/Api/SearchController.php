<?php

namespace App\Http\Controllers\Api;

use App\Lga;
use App\Bank;
use App\Education;
use App\AgroBasedCottage;
use App\AgricultureFacility;
use App\GovernmentAgency;
use App\Communication;
use App\Accessibility;
use App\Health;
use App\Water;
use App\Electricity;
use App\TourismRecreation;
use App\MineralBasedCottage;
use App\OtherSocialAmenity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function searchByText(Request $request){
        // First we define the error message we are going to show if no keywords
        // existed or if no results found.
        $error = ['error' => 'No results found, please try with different keywords.'];

        // Making sure the user entered a keyword.
        if (!is_null($request->input('q'))) {

            // Using the Laravel Scout syntax to search the products table.
            switch($request->has('q')){
                case 1:
                $bank = collect(Bank::search($request->get('q'))->get()->load('lga'));
                $edu = collect(Education::search($request->get('q'))->get()->load('lga'));
                $health = collect(Health::search($request->get('q'))->get()->load('lga'));
                $water = collect(Water::search($request->get('q'))->get()->load('lga'));
                $access = collect(Accessibility::search($request->get('q'))->get()->load('lga'));
                $electri = collect(Electricity::search($request->get('q'))->get()->load('lga'));
                $tourism = collect(TourismRecreation::search($request->get('q'))->get()->load('lga'));
                $agriculture = collect(AgricultureFacility::search($request->get('q'))->get()->load('lga'));
                $mineral = collect(MineralBasedCottage::search($request->get('q'))->get()->load('lga'));
                $othersoc = collect(OtherSocialAmenity::search($request->get('q'))->get()->load('lga'));
                $govern = collect(GovernmentAgency::search($request->get('q'))->get()->load('lga'));
                $communi = collect(Communication::search($request->get('q'))->get()->load('lga'));
                $agro = collect(AgroBasedCottage::search($request->get('q'))->get()->load('lga'));
                
                $merge1 = $bank->merge($edu);
                $merge2 = $merge1->merge($health);
                $merge3 = $merge2->merge($water);
                $merge4 = $merge3->merge($access);
                $merge5 = $merge4->merge($electri);
                $merge6 = $merge5->merge($tourism);
                $merge7 = $merge6->merge($agriculture);
                $merge8 = $merge7->merge($mineral);
                 $merge9 = $merge8->merge($othersoc);
                $merge10 = $merge9->merge($agro);
                $merge11 = $merge10->merge($govern);
                $posts = $merge11->merge($communi);
            }
            
            

            // If there are results return them, if none, return the error message.
            return $posts->count() ? $posts->all() : $error;

        }

        // Return the error message if no keywords existed
        return $error;
    }

    public function SearchIndustryByLga(Request $request)
    {
        // First we define the error message we are going to show if no keywords
        // existed or if no results found.
        $error = ['error' => 'No results found, please try with different keywords.'];

        // Making sure the user entered a keyword.
        //industry
        if ($request->has('lga') && $request->has('industry')) {

            // Using the Laravel Scout syntax to search the products table.
           // $posts = collect(Bank::search($request->get('q'))->get());
            $industries = $request->get('industry');
            $lga = (new Lga)->newQuery();
            $posts = $lga->whereId($request->get('lga'))->first()->$industries->load('lga');

            // If there are results return them, if none, return the error message.
            return $posts->count() ? $posts : $error;

        }

        // Return the error message if no keywords existed
        return $error;
    }

    public function SearchByGraph(Request $request)
    {
        if (!is_null($request->ownership) && !is_null($request->type)) {

            return $this->loadOnlyIndustryWithTypeAndOwnership($request->industry, $request->type, $request->ownership);

        }

        if (!is_null($request->ownership) && !is_null($request->industry)) {

            return $this->loadOnlyIndustryWithOwnership($request->industry, $request->ownership);

        }

        if (!is_null($request->type)) {

            return $this->loadOnlyIndustryWithType($request->industry, $request->type);

        }

        return $this->loadOnlyIndustry($request->industry);       

    }

    private function loadOnlyIndustry($industries){
        $count = Lga::all()->count();
        $lgaByIndustry = [];
         for ($i = 1; $i <= $count; $i++) {
            $lga = (new Lga)->newQuery();
            $industry = $lga->whereId($i)->first()->$industries;
            $value = $industry->count();
            array_push($lgaByIndustry, $value);
            }
        return collect($lgaByIndustry);

    }

    private function loadOnlyIndustryWithType($industries, $type){
        $count = Lga::all()->count();
        $lgaByIndustry = [];
        $type = str_replace("+"," ",$type);

         for ($i = 1; $i <= $count; $i++) {
            $lga = (new Lga)->newQuery();
            $industry = $lga->whereId($i)->first()->$industries
                ->where('category', $type);
            $value = $industry->count();
            array_push($lgaByIndustry, $value);
            }
        return collect($lgaByIndustry);

    }

    private function loadOnlyIndustryWithOwnership($industries, $ownership){
        $count = Lga::all()->count();
        $lgaByIndustry = [];
        $ownership = str_replace("+"," ",$ownership);

         for ($i = 1; $i <= $count; $i++) {
            $lga = (new Lga)->newQuery();
            $industry = $lga->whereId($i)->first()->$industries
                ->where('ownership_type', $ownership);
            $value = $industry->count();
            array_push($lgaByIndustry, $value);
            }
        return collect($lgaByIndustry);

    }

     private function loadOnlyIndustryWithTypeAndOwnership($industries, $type, $ownershipType){
        $count = Lga::all()->count();
        $lgaByIndustry = [];
        $type = str_replace("+"," ",$type);
        $ownershipType = str_replace("+"," ",$ownershipType);

         for ($i = 1; $i <= $count; $i++) {
            $lga = (new Lga)->newQuery();
            $industry = $lga->whereId($i)->first()->$industries
                ->where('category', $type)
                ->where('ownership_type', $ownershipType);
            $value = $industry->count();
            array_push($lgaByIndustry, $value);
            }
        return collect($lgaByIndustry);

    }

       
        
}
