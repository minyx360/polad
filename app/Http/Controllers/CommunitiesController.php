<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Community;
use App\Lga;

class CommunitiesController extends Controller
{
    //



    public function getCommunities(){


        $community = Community::with('lga')->get();
     //   dd($community->communities);
        $communities = $community;

        return view('communities', compact('communities', $communities));
    }

    public function searchForCommunity($id){

        $communities = Community::CommunityId($id)->get();
        //return $$communities;
        return view('communities',  compact('communities'));
    }

    public function search(Request $request){

        $error = ['error' => 'No result found'];


        if($request->has('q')){
            $posts = Community::search($request->get('q'))->get();

            // If there are results return them, if none, return the error message.
            return $posts->count() ? $posts : $error;
        }

        return $error;
    }
}
