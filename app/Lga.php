<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Lga extends Model
{
    use \Znck\Eloquent\Traits\BelongsToThrough;

    use Searchable;

    public function toSearchableArray()
    {
        return [
            'name' => $this->name
        ];
    }


    public function communities(){

        return $this->hasMany(Community::class);
    }

    public function banks(){

        return $this->hasManyThrough(Bank::class, Community::class);
    }

    public function educations(){

        return $this->hasManyThrough(Education::class, Community::class);
    }

    public function electricities(){

        return $this->hasManyThrough(Electricity::class, Community::class);
    }

    public function accessibilities(){

        return $this->hasManyThrough(Accessibility::class, Community::class);
    }

    public function healths(){

        return $this->hasManyThrough(Health::class, Community::class);
    }

    public function communications(){

        return $this->hasManyThrough(Communication::class, Community::class);
    }

    public function waters(){

        return $this->hasManyThrough(Water::class, Community::class);
    }

    public function agroBasedCottages(){

        return $this->hasManyThrough(AgroBasedCottage::class, Community::class);
    }

    public function agricultureFacilities(){

        return $this->hasManyThrough(AgricultureFacility::class, Community::class);
    }

    public function mineralBasedCottages(){

        return $this->hasManyThrough(MineralBasedCottage::class, Community::class);
    }

    public function otherSocialAmenities(){

        return $this->hasManyThrough(OtherSocialAmenity::class, Community::class);
    }

    public function tourismRecreations(){

        return $this->hasManyThrough(TourismRecreation::class, Community::class);
    }

    public function governmentAgencies(){

        return $this->hasManyThrough(GovernmentAgency::class, Community::class);
    }







}
