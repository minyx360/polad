<?php

namespace App;

use App\Community;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    use \Znck\Eloquent\Traits\BelongsToThrough;
    use Searchable;

    protected $fillable = [
        'community_id', 'name', 'type', 'category', 'ownership_code', 'ownership', 'year_of_establish', 'students', 'teachers', 'picture_path'
    ];

    public function toSearchableArray()
    {
        return [
            'name' => $this->name,
            'category' => $this->category,
            'ownership_type' => $this->ownership_type,
            'community_id' => $this->community_id,

        ];
    }

    public function getCommunityIdAttribute($value)
    {
        $community = Community::find($value);
        return $community->name;
    }

    public function setCommunityIdAttribute($value)
    {
        // query community table to data attribute and set it community id
        $community = Community::where('name', $value)->first();
        // dd($community->id);
        return $this->attributes['community_id'] = $community->id;
    }

    public function community(){

        return $this->belongsTo(Community::class);
    }

    public function lga() {
        return $this->belongsToThrough(Lga::class, Community::class);
    }

    public function getCommunity($nameId){
        $allName = Community::all();
        // dd($allName);
        foreach ($allName as $comName){

            if ( $comName->name == $nameId){
                return $comName->id;
            }
        }
    }
}
