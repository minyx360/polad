<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Electricity extends Model
{
    use \Znck\Eloquent\Traits\BelongsToThrough;
    use Searchable;


    protected $fillable = [
        'community_id', 'point_x', 'point_y', 'ownership_code', 'ownership', 'year_of_establish', 'status_code', 'status'
    ];

    public function toSearchableArray()
    {
        return [
            'community_id' => $this->community_id,
        ];
    }

    public function getCommunityIdAttribute($value)
    {
        $community = Community::find($value);
        return $community->name;
    }

    public function setCommunityIdAttribute($value)
    {
        // query community table to data attribute and set it community id
        $community = Community::where('name', $value)->first();
        // dd($community->id);
        return $this->attributes['community_id'] = $community->id;
    }

    public function community(){

        return $this->belongsTo(Community::class);
    }

    public function lga()
    {
        return $this->belongsToThrough(Lga::class, Community::class);
    }
}
