<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class OtherSocialAmenity extends Model
{
    use \Znck\Eloquent\Traits\BelongsToThrough;
    use Searchable;

    protected $fillable = [
        'community_id', 'point_x', 'point_y', 'name', 'type', 'amenities_type',  'ownership', 'ownership_code', 'year_of_establish',  'image_path'
    ];

     public function toSearchableArray()
    {
        return [
            'name' => $this->name,
            'category' => $this->category,
            'ownership_type' => $this->ownership_type,
            'community_id' => $this->community_id,
        ];
    }

    public function getCommunityIdAttribute($value)
    {
        $community = Community::find($value);
        return $community->name;
    }

    public function setCommunityIdAttribute($value)
    {
        // query community table to data attribute and set it community id
        $community = Community::where('name', $value)->first();
        // dd($community->id);
        return $this->attributes['community_id'] = $community->id;
    }

    public function community(){

        return $this->belongsTo(Community::class);
    }

    public function lga()
    {
        return $this->belongsToThrough(Lga::class, Community::class);
    }
}
