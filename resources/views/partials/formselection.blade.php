<div class="section no-pad-bot" id="index-banner">
    <div class="container">
        <form method="post" action="">
            <div class="row center">
                <select class="browser-default" name="charts">
                    <option value="bar">Bar</option>
                    <option value="line">Line</option>
                    <option value="pie">Pie</option>
                </select>
            </div>



            <div class="row center">
                <select class="browser-default" id="chart" name="chart">
                    <option value="" disabled selected>Choose Graph Type</option>
                    <option value="Bar">Bar</option>
                    <option value="Line">Line</option>
                    <option value="3">Option 3</option>
                </select>
            </div>
            <div class="row center">

                <button href="" id="download-button" class="btn-large waves-effect waves-light orange"><i class="material-icons left">cloud</i>Graph</button>
            </div>
            <br><br>
        </form>

    </div>
</div>