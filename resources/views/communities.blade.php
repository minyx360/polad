@extends('layouts.master')
@section('myScript')
    <link href=" https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

@endsection

@section('header')
@include('partials.header')
@endsection
@section('content')
 @foreach($communities as $community)
<div class="section white">
    <div class="row container">
        <div class="row">
            <div container>
                <div class="col s12">
                    <div class="col s8">
                        <h2 class="header">{{$community->name . " Community"}}</h2>
                        <h2 class="flow-text">{{$community->lga_id . " LGA"}}  </h2>
                    </div>
                    <div class="col s4">
                        <img class="materialboxed" width="350" src="{{url('/storage/ado-ekiti/'. $community->image_path)}}">
                    </div>
                    <div class="col s6">
                        <ul class="collection">
                            <li class="collection-item"><i class="{{ $community->education->count() ? 'fa fa-check' : 'fa fa-close' }}" aria-hidden="true"></i> Schools  <span style="color: #FFFFFF;" class=" badge {{ $community->education->count() ? 'red' : 'yellow' }}">{{$community->education->count()}}</span></li>
                            <li class="collection-item"><i class="{{ $community->electricity->count() ? 'fa fa-check' : 'fa fa-close' }}" aria-hidden="true"></i> Electricity :  <span style="color: #FFFFFF;" class=" badge {{ $community->electricity->count() ? 'red' : 'yellow' }}">{{$community->electricity->count()}}</span> </li>
                            <li class="collection-item"><i class="{{ $community->health->count() ? 'fa fa-check' : 'fa fa-close' }}" aria-hidden="true"></i> Hospital :  <span style="color: #FFFFFF;" class=" badge {{ $community->health->count() ? 'red' : 'yellow' }}">{{$community->health->count()}}</span> </li>
                            <li class="collection-item"><i class="{{ $community->communication->count() ? 'fa fa-check' : 'fa fa-close' }}" aria-hidden="true"></i> Communication : <span style="color: #FFFFFF;" class=" badge {{ $community->communication->count() ? 'red' : 'yellow' }}">{{$community->communication->count()}}</span> </li>
                            <li class="collection-item"><i class="{{ $community->agricultureFacility->count() ? 'fa fa-check' : 'fa fa-close' }}" aria-hidden="true"></i>  Agriculture Facilities :  <span style="color: #FFFFFF;" class=" badge {{ $community->agricultureFacility->count() ? 'red' : 'yellow' }}">{{$community->agricultureFacility->count()}} </span></li>
                            <li class="collection-item"><i class="{{ $community->mineralBasedCottage->count() ? 'fa fa-check' : 'fa fa-close' }}" aria-hidden="true"></i> Mineral-Based Cottage : <span style="color: #FFFFFF;" class=" badge {{ $community->mineralBasedCottage->count() ? 'red' : 'yellow' }}">{{$community->mineralBasedCottage->count()}} </span></li>
                            <li class="collection-item"><i class="{{ $community->governmentAgency->count() ? 'fa fa-check' : 'fa fa-close' }}" aria-hidden="true"></i> Government Agencies : <span style="color: #FFFFFF;" class="badge {{ $community->governmentAgency->count() ? 'red' : 'yellow' }}">{{$community->governmentAgency->count()}}</span> </li>

                        </ul>

                    </div>
                    <div class="col s6">
                        <ul class="collection">
                            <li class="collection-item"><i class="{{ $community->water->count() ? 'fa fa-check' : 'fa fa-close' }}" aria-hidden="true"></i> Water : <span style="color: #FFFFFF;" class="badge {{ $community->water->count() ? 'red' : 'yellow' }}">{{$community->water->count()}}</span> </li>
                            <li class="collection-item"><i class="{{ $community->banks->count() ? 'fa fa-check' : 'fa fa-close' }}" aria-hidden="true"></i> Banks : <span style="color: #FFFFFF;" class="badge {{ $community->banks->count() ? 'red' : 'yellow' }}">{{$community->banks->count()}}</span> </li>
                            <li class="collection-item"><i class="{{ $community->accessibility->count() ? 'fa fa-check' : 'fa fa-close' }}" aria-hidden="true"></i>  Accessibility :  <span style="color: #FFFFFF;" class="badge {{ $community->accessibility->count() ? 'red' : 'yellow' }}">{{$community->accessibility->count()}}</span> </li>
                            <li class="collection-item"><i class="{{ $community->agroBasedCottage->count() ? 'fa fa-check' : 'fa fa-close' }}" aria-hidden="true"></i> Agro-Based Cottage : <span style="color: #FFFFFF;" class="badge {{ $community->agroBasedCottage->count() ? 'red' : 'yellow' }}">{{$community->agroBasedCottage->count()}} </span></li>
                            <li class="collection-item"><i class="{{ $community->otherSocialAmenity->count() ? 'fa fa-check' : 'fa fa-close' }}" aria-hidden="true"></i>  Other social Amenities :  <span style="color: #FFFFFF;" class="badge {{ $community->otherSocialAmenity->count() ? 'red' : 'yellow' }}">{{$community->otherSocialAmenity->count()}}</span> </li>
                            <li class="collection-item"><i class="{{ $community->tourismRecreation->count() ? 'fa fa-check' : 'fa fa-close' }}" aria-hidden="true"></i> Tourism & Recreation : <span style="color: #FFFFFF;" class="badge {{ $community->tourismRecreation->count() ? 'red' : 'yellow' }}">{{$community->tourismRecreation->count()}}</span> </li>


                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

   
       <div class="section">
          <div class="container">
           
          </div>
    </div>
    @endforeach
@endsection