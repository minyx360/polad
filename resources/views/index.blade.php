@extends('layouts.master')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col s12">
                <div class="row add-height valign-wrapper">
                    <div class="col s4">
                        <div class="center promo promo-example card-panel hoverable" >
                            <a href="{{ route('search') }}">
                                <i class="large material-icons">search</i>
                                <p class="promo-caption">Search</p>
                        </div>
                    </div>
                    <div class="col s4">
                        <div class="center promo promo-example card-panel hoverable" >
                            <a href="{{ route('lga') }}">
                                <i class="large material-icons">zoom_in</i>
                                <p class="promo-caption">Search By Lga</p>
                        </div>
                    </div>

                    <div class="col s4">
                        <div class="center promo promo-example card-panel hoverable" >
                            <a href="{{ route('graph') }}">
                                <i class="large material-icons">insert_chart</i>
                                <p class="promo-caption">Search by Graph</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection