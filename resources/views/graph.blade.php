@extends('layouts.master')

@section('header')
@include('partials.header')
@endsection

@section('content')

    <div id="app">
        <search-by-graph></search-by-graph>
    </div>

@endsection
