<!DOCTYPE html>
<html lang="en">
    <head>
        <title> Polad Technologies Limited</title>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!--Import Google Icon Font-->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="{{url('css/materialize.min.css')}}"  media="screen,projection"/>
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <script>
            $(document).ready(function(){
                $('.materialboxed').materialbox();
            });
        </script>
        <style type="text/css">
            body {
                    display: flex;
                    min-height: 100vh;
                    flex-direction: column;
                  }

            main {
                    flex: 1 0 auto;
                  }
            .add-height{
                height: 500px;
            }

            .parallax-container {
                height: 300px;
            }
        </style>
        
        @yield('myScript')
    </head>
  
    <body>
        <header>
            @yield('header')
        </header>
    	<main>
            
                @yield('content')
            
    	</main>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
        <script type="text/javascript" src="{{url('js/materialize.min.js')}}"></script>
        <script src="{{asset('/js/app.js')}}"></script>
    </body>
</html>
