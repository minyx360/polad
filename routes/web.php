<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});



Route::get('/search-by-lga', 'LgaController@searchByLga')->name('searchByLga');
Route::get('/search', function () {
    return view('search');
})->name('search');
Route::get('/lga', function () {
    return view('lga');
})->name('lga');
Route::get('/graph', function () {
    return view('graph');
})->name('graph');

Route::get('/community/{id}', 'CommunityController@CommunityInfo');
Route::get('/communities/{id}', 'LgaController@searchForCommunity')->name('community');
Route::get('/communities/{id}', 'CommunitiesController@searchForCommunity');
Route::get('/searching', 'LgaController@searching')->name('searching');
Route::get('/industries', 'LgaController@getIdustries')->name('industries');
Route::get('/searching-lga', 'LgaController@searchingLga')->name('searching-lga');