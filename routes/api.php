<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/search', 'Api\SearchController@searchByText')->name('api.searchByText');
Route::get('/lga', 'Api\SearchController@searchBylga')->name('api.searchByLga');

Route::get('/list-industry', 'Api\DataListController@getIndustryList')->name('api.getIndustryList');
Route::get('/list-lga', 'Api\DataListController@getLgaList')->name('api.getLgaList');
Route::get('/search-industry-by-lga', 'Api\SearchController@SearchIndustryByLga')->name('api.SearchIndustryByLga');
Route::get('/search-by-lga', 'Api\SearchController@SearchByGraph')->name('api.SearchByGraph');
Route::get('/industry-category/{industry}', 'Api\DataListController@getIndustryCategory')->name('api.getIndustryCategory');
Route::get('/industry-ownership/{ownership}', 'Api\DataListController@getOwnershipType')->name('api.getOwnershipType');
